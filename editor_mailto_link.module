<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Implements hook_FORM_ID_form_alter().
 */
function editor_mailto_link_form_editor_link_dialog_alter(&$form, FormStateInterface $form_state) {
  $user_input = $form_state->getUserInput();
  $input = $user_input['editor_object'] ?? [];

  $url_field = _editor_mailto_link_parse_url_field($input['href'] ?? NULL);

  $form['link_type'] = [
    '#type' => 'select',
    '#title' => t('Link type'),
    '#options' => [
      'link' => t('Link'),
      'mailto' => t('Mailto'),
      'tel' => t('Telephone'),
    ],
    '#default_value' => $url_field['type'],
    '#weight'=> -333,
  ];

  $form['mailto'] = [
    '#type' => 'fieldset',
    '#title' => t('Mailto'),
    '#states' => [
      'visible' => [
        'select[name="link_type"]' => ['value' => 'mailto'],
      ],
    ],
  ];

  $form['attributes']['href']['#states'] = [
    'visible' => [
      'select[name="link_type"]' => ['value' => 'link'],
    ],
  ];

  $form['mailto']['email'] = [
    '#title' => t('Email'),
    '#type' => 'email',
    '#default_value' => $url_field['mailto']['email'] ?? '',
    '#maxlength' => 2048,
  ];
  $form['mailto']['subject'] = [
    '#title' => t('Subject'),
    '#type' => 'textfield',
    '#default_value' => $url_field['mailto']['subject'] ?? '',
    '#maxlength' => 2048,
  ];
  $form['mailto']['body'] = [
    '#title' => t('Body'),
    '#type' => 'textarea',
    '#default_value' => $url_field['mailto']['body'] ?? '',
    '#maxlength' => 2048,
  ];

  $form['tel'] = [
    '#title' => t('Telephone number'),
    '#type' => 'textfield',
    '#default_value' => $url_field['tel']['number'] ?? '',
    '#maxlength' => 2048,
    '#states' => [
      'visible' => [
        'select[name="link_type"]' => ['value' => 'tel'],
      ],
    ],
  ];

  $form['#validate'][] = '_editor_mailto_link_submit_handler';
}

function _editor_mailto_link_parse_url_field(?string $href) {
  if (preg_match('/mailto:(.*?)\?(.*)/', $href, $matches)) {
    parse_str($matches[2], $output);

    return [
      'type' => 'mailto',
      'mailto' => [
        'email' => $matches[1],
        'body' => $output['body'] ?? NULL,
        'subject' => $output['subject'] ?? NULL,
      ],
    ];
  }

  if (preg_match('/tel:(.*)/', $href, $matches)) {
    return [
      'type' => 'tel',
      'tel' => [
        'number' => $matches[1],
      ],
    ];
  }

  return [
    'type' => 'link',
    'href' => $href,
  ];
}


function _editor_mailto_link_submit_handler(array &$form, FormStateInterface $form_state) {
  $form_values = $form_state->getValues();

  if ($form_values['link_type'] == 'mailto') {
    $form_state->setValue('attributes', ['href' => 'mailto:' . _editor_mailto_link_build_mailto_link($form_values['mailto'])]);
  }

  if ($form_values['link_type'] == 'tel') {
    $form_state->setValue('attributes', ['href' => 'tel:' . $form_values['tel']]);
  }
}


function _editor_mailto_link_build_mailto_link(array $mailto_values) {
  $ret = $mailto_values['email'];

  $params = [];
  if ($mailto_values['subject']) {
    $params['subject'] = $mailto_values['subject'];
  }
  if ($mailto_values['body']) {
    $params['body'] = $mailto_values['body'];
  }

  if ($params) {
    $ret .= '?' . UrlHelper::buildQuery($params);
  }

  return $ret;
}
